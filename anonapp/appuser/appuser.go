package appuser

import (
	"appengine"
	"appengine/datastore"
	"html/template"
	"net/http"
	"time"
)

type TextInfo struct {
	Index    string
	Summery  string
	Text     []byte
	Created  time.Time
	Modified time.Time
}

type TopContent struct {
	Title     string
	TextInfos []TextInfo
}

type SingleContent struct {
	Title   string
	Text    string
	Index   string
	Created time.Time
}

/*
Handler for Access to Index Page.
*/
func IndexHandle(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	q := datastore.NewQuery("Textinfo").Order("-Created").Limit(10)
	ti := make([]TextInfo, 0, 10)
	if _, err := q.GetAll(c, &ti); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	tc := TopContent{
		Title:     "TopPage",
		TextInfos: ti,
	}

	if err := toppageTemplate.Execute(w, tc); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func TextHandle(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	path := r.URL.Path
	path = path[1:len(path)]
	q := datastore.NewQuery("Textinfo").Filter("Index =", path)
	ti := make([]TextInfo, 0, 1)
	if _, err := q.GetAll(c, &ti); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	si := SingleContent{
		Title:   "/" + path,
		Text:    string(ti[0].Text),
		Index:   ti[0].Index,
		Created: ti[0].Created,
	}
	if err := singlepageTemplate.Execute(w, si); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

var toppageTemplate = template.Must(template.New("toppage").Parse(TopPage))

const TopPage = `
<html>
	<head>
  	<title>{{.Title}}</title>
  	</head>
  	<body>
  		<h1 id="title">Your Anon Text by Golang.</h1>
  		{{range .TextInfos}}
  			<div class="textlist">
        		<p>An anonymous person wrote:</p>
      			<pre>{{.Summery}}</pre>
      			<p><a href="/{{.Index}}">{{.Created}}</a></p>
      		</div>
    	{{end}}
  		<form action="/write" method="post">
      		<div><textarea name="content" rows="3" cols="60"></textarea></div>
      		<div><input type="submit" value="Post"></div>
    	</form>
  	</body>
</html>
`

var singlepageTemplate = template.Must(template.New("singlepage").Parse(SinglePage))

const SinglePage = `
<html>
	<head>
  	<title>{{.Title}}</title>
  	</head>
  	<body>
  		<h1 id="title">Your Anon Text by Golang.</h1>
  			<div class="textlist">
        		<p>An anonymous person wrote:</p>
      			<pre>{{.Text}}</pre>
      			<p><a href="/{{.Index}}">{{.Created}}</a></p>
      		</div>
  	</body>
</html>
`

func create62BaseString(n int, base int) string {
	var (
		r      int
		s      string
		digits string
	)
	digits = "abcdefghijklABCDEFGHIJK12345mnopqrstuvwxyz67890LMNOPQRSTUVWXYZ"

	if n < 0 || base < 2 || base > 62 {
		return ""
	}

	for n > 0 {
		r = n % base
		s += string(digits[r])
		n /= base
	}

	return s
}

func substr(s string, pos, length int) string {
	ints := []byte(s)
	l := pos + length
	if l > len(ints) {
		l = len(ints)
	}
	return string(ints[pos:l])
}

func WriteText(w http.ResponseWriter, r *http.Request) {
	c := appengine.NewContext(r)
	q := datastore.NewQuery("Textinfo")
	count, e1 := q.Count(c)
	if e1 != nil {
		http.Error(w, e1.Error(), http.StatusInternalServerError)
		return
	}
	smr := ""
	if len(r.FormValue("content")) > 140 {
		smr = substr(r.FormValue("content"), 0, 140) + "..."
	} else {
		smr = r.FormValue("content")
	}
	ti := TextInfo{
		Index:    create62BaseString(count+10000, 62),
		Summery:  smr,
		Text:     []byte(r.FormValue("content")),
		Created:  time.Now(),
		Modified: time.Now(),
	}

	_, err := datastore.Put(c, datastore.NewIncompleteKey(c, "Textinfo", nil), &ti)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/", http.StatusFound)
}
