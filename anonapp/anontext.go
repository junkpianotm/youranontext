package anonapp

import (
	"anonapp/appuser"
	"net/http"
)

func init() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" {
			appuser.IndexHandle(w, r)
		} else {
			if r.URL.Path == "/write" && r.Method == "POST" {
				appuser.WriteText(w, r)
			} else {
				appuser.TextHandle(w, r)
			}
		}
	})
}
